defmodule Elinterfaces do
@moduledoc """ 
ElInterfaces is a personal wrapper around inet:'s interfaces methods.
It should be fun.
It should be useful too.
No yet.
"""

@doc "Returns a list containing the interfaces which are currently up"
  def listIfs do
    # Get alive interfaces
    {_, interfaces} = :inet.getiflist
    # Print them one per line.
    for if <- interfaces do 
      if
    end
  end

@doc """
Returns a list containing the interfaces and their IP addresses (v4 & v6)
## Example
iex(netowrk@Elixir)1> ElInterfaces.ifaces_ips
[lo: [{127, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 0, 1}], eth1: [],
 wlan1: [{192, 168, 1, 39},
  {10754, 33824, 28836, 54016, 12912, 22817, 11029, 44318},
  {65152, 0, 0, 0, 3828, 20032, 15221, 32077},
  {65152, 0, 0, 0, 57862, 59135, 65220, 42934}], mon0: [],
 ]


"""
  def ifaces_ips do
    {_, _if_ips} = :inet.getifaddrs()
    if_ips =  Enum.map(_if_ips, fn({if, data}) -> 
      {List.to_atom(if), Keyword.get_values(data, :addr)}
    end)
  end

@doc """
Displays in a fancy manner the interfaces and their IP addresses (v4 & v6)
## Example : 
iex(netowrk)5> ElInterfaces.print_ips          
:lo has ips: ['127.0.0.1', '::1']
:eth1 has ips: []
:wlan1 has ips: ['192.168.1.39', '2A02:8420:70A4:D300:3270:5921:2B15:AD1E', 'FE80::EF4:4E40:3B75:7D4D', 'FE80::E206:E6FF:FEC4:A7B6']
:mon0 has ips: []
:ok
"""
  def print_ips do
    for {iface, ips} <- ifaces_ips do
      list_ips = Enum.map(ips, &:inet.ntoa/1)
      IO.puts(inspect(iface) <> " has ips: "  <> inspect(list_ips))
    end
    :ok 
  end

@doc """
Returns `true` when `if` has an IP address, or false.
"""
@spec hasIp?(atom) :: true | false
  def hasIp?(if) when is_atom(if) do
    bool_format_if_data(ifaces_ips[if])
  end
   
@doc """
Returns `true` when `if` has an IP address, or a two-elements tuple {:error, <reason>},
<reason> being either `:no_if` or `:no_ip`.
"""
@spec hasIp(atom) :: true | false
  def hasIp(if) when is_atom(if) do
    format_if_data(ifaces_ips[if])
  end


####
# Internal API
####


defp format_if_data(nil) do {:error, :no_if} end
defp format_if_data([]) do {:error, :no_ip} end
defp format_if_data(ip) do true end

defp bool_format_if_data(nil) do false end
defp bool_format_if_data([]) do false end
defp bool_format_if_data(ip) do true end


end
