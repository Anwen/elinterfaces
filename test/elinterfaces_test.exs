defmodule ElinterfacesTest do
  use ExUnit.Case

  test ":inet Availability" do
    :inet.getiflist()
    :inet.getifaddrs()
  end
end
