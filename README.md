# ElInterfaces

My own Elixir wrapper around the Erlang network libraries.

## Functions
* listIfs/0
* ifaces_ips/0
* print_ips/0
* hasIp/1
* hasIp?/1

